#!/bin/bash

HOME_DIR=$(pwd)
export CI=1

sed -i "s|placeholder|$BOT_TOKEN|g" telegram-send.conf
mkdir $HOME/.config
mv telegram-send.conf $HOME/.config/telegram-send.conf

git clone https://github.com/Neutron-Toolchains/clang-build.git

mv $HOME_DIR/push_ci.sh $HOME_DIR/clang-build/push_ci.sh

cd clang-build

bash build_clang.sh && bash build_binutils.sh && bash post_build.sh && bash push_ci.sh
